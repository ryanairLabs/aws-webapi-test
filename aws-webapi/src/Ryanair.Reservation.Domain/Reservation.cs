﻿using System.Collections.Generic;
using System.Linq;

namespace Ryanair.Reservation.Domain
{
    public class Reservation
    {
        public int Id { get; private set; }

        public string ReservationNumber { get; internal set; }

        public string Email { get; internal set; }

        public string CreditCard { get; internal set; }

        public Flight InboundFlight { get; internal set; }

        public Flight OutboundFlight { get; internal set; }
        
        public List<Passenger> PassengersInboundFlight { get; private set; }

        public List<Passenger> PassengersOutboundFlight { get; private set; }

        public bool IsRoundTrip => this.InboundFlight != null;
    }
}
