﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ryanair.Reservation.ApplicationService;
using Ryanair.Reservation.ApplicationService.Interfaces;
using Ryanair.Reservation.Repositories;

namespace Ryanair.Reservation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen();

            this.ConfigureRepositories(services);
            this.ConfigureApplicationServices(services);

            services.AddMvcCore().AddApiExplorer();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BlueETL Reservations API");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        private void ConfigureRepositories(IServiceCollection services)
        {
            services.AddSingleton<IFlightRepository, FlightRepository>();
            services.AddSingleton<IReservationRepository, ReservationRepository>();
        }

        private void ConfigureApplicationServices(IServiceCollection services)
        {
            services.AddScoped<IFlightQueryService, FlightQueryService>();
            services.AddScoped<IReservationService, ReservationService>();
            services.AddScoped<IReservationQueryService, ReservationQueryService>();
        }
    }
}
