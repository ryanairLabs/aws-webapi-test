﻿namespace Ryanair.Reservation.Models
{
    public class Passenger
    {
        public string Name { get; set; }
        public int Bags { get; set; }
        public string Seat { get; set; }

        public Passenger(string name, int bags, string seat)
        {
            this.Name = name;
            this.Seat = seat;
            this.Bags = bags;
        }
    }
}
