﻿using System.Collections.Generic;

namespace Ryanair.Reservation.Models
{
    public class Reservation
    {
        public string ReservationNumber { get; set; }

        public string Email { get; set; }

        public List<ReservationFlight> Flights { get; set; }
    }
}
