﻿using Ryanair.Reservation.Domain;
using System;
using System.Collections.Generic;

namespace Ryanair.Reservation.Repositories
{
    public interface IFlightRepository
    {
        IEnumerable<Flight> Get(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip);
    }
}
