﻿using Ryanair.Reservation.Domain;
using Ryanair.Reservation.Repositories;
using System.Collections.Generic;
using Ryanair.Reservation.ApplicationService.Interfaces;

namespace Ryanair.Reservation.ApplicationService
{
    //
    // Rename, reorder refactor is allowed. Do whatever you think is the best choice.
    //

    public class ReservationService : IReservationService
    {
        private readonly IFlightRepository _flightRepository;
        private readonly IReservationRepository _reservationRepository;

        public ReservationService(IFlightRepository flightRepository, IReservationRepository reservationRepository)
        {
            _flightRepository = flightRepository;
            _reservationRepository = reservationRepository;
        }

        public Domain.Reservation BookOneWayTrip(string email, string creditCard, string outboundFlightKey, List<Passenger> outboundPassengers, string inboundFlightKey, List<Passenger> inboundPassengers)
        {
            // TODO: Implement the method

            return null;
        }

        public Domain.Reservation BookRoundTrip(string email, string creditCard, string flightKey, List<Passenger> passengers)
        {
            // TODO: Implement the method

            return null;
        }
    }
}
