﻿namespace Ryanair.Reservation.ApplicationService.Interfaces
{
    public interface IReservationQueryService
    {
        Domain.Reservation GetByReservationKey(string reservationKey);
    }
}
