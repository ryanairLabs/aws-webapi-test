﻿using System.Collections.Generic;
using Ryanair.Reservation.Domain;

namespace Ryanair.Reservation.ApplicationService.Interfaces
{
    public interface IReservationService
    {
        Domain.Reservation BookOneWayTrip(string email, string creditCard, string outboundFlightKey, List<Passenger> outboundPassengers, string inboundFlightKey, List<Passenger> inboundPassengers);

        Domain.Reservation BookRoundTrip(string email, string creditCard, string flightKey, List<Passenger> passengers);
    }
}
