﻿using System;
using System.Collections.Generic;
using Ryanair.Reservation.Domain;

namespace Ryanair.Reservation.ApplicationService.Interfaces
{
    public interface IFlightQueryService
    {
        List<Flight> Get(int passengers, string origin, string destination, DateTime dateOut, DateTime dateIn, bool roundTrip);
    }
}
